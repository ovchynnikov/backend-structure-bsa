const betService = require('../services/bet.service.js');
const eventService = require('../services/event.service.js');
const userService = require('../services/user.service.js');
const { createOdds } = require('../services/odds.service.js');

const createEvent = async(req, res, next) => {
    try {
        const odds = await createOdds(req.body.odds);

        const event = await eventService.createEvent({
            ...req.body,
            oddsId: odds.id,
        });

        req.app.get('statEmitter').emit('newEvent');

        return res.send({...event, odds });
    } catch (error) {
        next(error);
    }
};

const updateEventById = async(req, res, next) => {
    try {
        const bets = await betService.getWinBetsByEventId(req.params.id, null);

        let multiplier;
        switch (req.body.prediction) {
            case 'w1':
                multiplier = odds.home_win;
                break;
            case 'w2':
                multiplier = odds.away_win;
                break;
            case 'x':
                multiplier = odds.draw;
                break;
        }
        const event = await eventService.updateEvent(req.params.id, { score: req.body.score });

        for (const bet of bets) {
            if (bet.prediction === multiplier) {
                const user = await userService.getUserById(bet.userId);

                await betService.updateBetById(bet.id, { win: true });
                await userService.updateUser(user.id, { balance: user.balance + bet.betAmount * bet.multiplier });

                continue;
            }

            betService.updateBetById(bet.id, { win: false });
        }

        return res.send(event);
    } catch (error) {
        next(error);
    }
};

module.exports = { createEvent, updateEventById };