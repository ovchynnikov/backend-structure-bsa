const userService = require('../services/user.service.js');

const getUserById = async(req, res, next) => {
    try {
        const user = await userService.getUserById(req.params.id);

        if (!user) {
            res.status(404).send({ error: 'User not found' });
        }

        return res.send(user);
    } catch (error) {
        next(error)
    }
};

const createUser = async(req, res, next) => {
    try {
        const user = await userService.createUser(req.body);

        req.app.get('statEmitter').emit('newUser');

        return res.send(user);
    } catch (error) {
        next(error)
    }
};

const updateUserById = async(req, res, next) => {
    try {
        const user = await userService.updateUser(req.params.id, req.body);
        return res.send(user);
    } catch (error) {
        next(error)
    }
};

module.exports = { getUserById, createUser, updateUserById };