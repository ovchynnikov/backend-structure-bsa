const Router = require('express');
const jwt = require("jsonwebtoken");
const { stats } = require('../../stats/stats.js');
const router = Router();

router.get("/", (req, res) => {
    console.log('req.headers', req.headers);
    try {
        let token = req.headers['authorization'];
        console.log('token', token);
        if (!token) {
            return res.status(401).send({ error: 'Not Authorized' });
        }
        token = token.replace('Bearer ', '');
        try {
            let tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
            console.log('tokenPayload', tokenPayload, (tokenPayload.type != 'admin'));
            if (tokenPayload.type != 'admin') {
                throw new Error();
            }
        } catch (err) {
            return res.status(401).send({ error: 'Not Authorized' });
        }
        res.send(stats);
    } catch (err) {
        res.status(500).send("Internal Server Error");
        return;
    }
});

module.exports = router;