const Router = require('express');
// const jwt = require("jsonwebtoken");
const { getSchema } = require('../../db/joi/users/joi.schema.js');
const { postSchema } = require('../../db/joi/users/joi.schema.js');
const { putSchema } = require('../../db/joi/users/joi.schema.js');
// const { db } = require("../../db/connect.js");
const userController = require('../../api/controllers/user.controller.js');
const { validationMiddleware } = require('../middlewares/validation.middleware.js');
const { authMiddleware } = require('../middlewares/auth.middleware.js');

const router = Router();

router.get("/:id", validationMiddleware(getSchema, 'params'), userController.getUserById);

router.post("/", validationMiddleware(postSchema, 'body'), userController.createUser);

router.put("/:id", validationMiddleware(putSchema, 'body'), authMiddleware, userController.updateUserById);


module.exports = router;