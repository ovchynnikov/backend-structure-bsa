const healthRoutes = require('./health.routes.js');
const usersRoutes = require('./users.routes.js');
const eventsRoutes = require('./events.routes.js');
const betsRoutes = require('./bets.routes.js');
const statsRoutes = require('./stats.routes.js');
const transactionsRoutes = require('./transactions.routes.js');


module.exports = app => {
    app.use('/bets', betsRoutes);
    app.use('/events', eventsRoutes);
    app.use('/health', healthRoutes);
    app.use('/users', usersRoutes);
    app.use('/stats', statsRoutes);
    app.use('/transactions', transactionsRoutes);
}