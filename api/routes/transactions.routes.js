const Router = require('express');
const jwt = require("jsonwebtoken");
const { postSchema } = require('../../db/joi/transactions/joi.schema.js');
const { db } = require("../../db/connect.js");

const router = Router();

router.post("/", (req, res) => {

    const isValidResult = postSchema.validate(req.body);
    if (isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
    };

    let token = req.headers['authorization'];
    if (!token) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
        let tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        if (tokenPayload.type != 'admin') {
            throw new Error();
        }
    } catch (err) {
        return res.status(401).send({ error: 'Not Authorized' });
    }

    db("user").where('id', req.body.userId).then(([user]) => {
        if (!user) {
            res.status(400).send({ error: 'User does not exist' });
            return;
        }
        req.body.card_number = req.body.cardNumber;
        delete req.body.cardNumber;
        req.body.user_id = req.body.userId;
        delete req.body.userId;
        db("transaction").insert(req.body).returning("*").then(([result]) => {
            const currentBalance = req.body.amount + user.balance;
            db("user").where('id', req.body.user_id).update('balance', currentBalance).then(() => {
                ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {
                    // const index = whatakey.indexOf('_');
                    // const newKey = whatakey.replace('_', '');
                    // newKey = newKey.split('')
                    // newKey[index] = newKey[index].toUpperCase();
                    // newKey = newKey.join('');
                    const { newKey } = stringModifier(whatakey);
                    result[newKey] = result[whatakey];
                    delete result[whatakey];
                })
                return res.send({
                    ...result,
                    currentBalance,
                });
            });
        });
    }).catch(err => {
        res.status(500).send("Internal Server Error");
        return;
    });
});

module.exports = router;