const jwt = require('jsonwebtoken');
const { secret_jwt } = require('../../config/jwt.config.js');

exports.authMiddleware = (req, res, next) => {

    let token = req.headers['authorization'];
    let tokenPayload;
    if (!token) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
        tokenPayload = jwt.verify(token, secret_jwt);
    } catch (err) {
        return res.status(401).send({ error: 'Not Authorized' });
    }

    if (req.params.id !== tokenPayload.id) {
        return res.status(401).send({ error: 'UserId mismatch' });
    }
    next()
}