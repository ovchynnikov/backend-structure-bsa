exports.validationMiddleware = (currSchema, currValue) => (req, res, next) => {
    let isValidResult = currSchema.validate(req[currValue]);
    if (isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
    }
    next();
}