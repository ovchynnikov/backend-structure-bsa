const jwt = require('jsonwebtoken');
const { jwt_secret } = require('../../config/jwt.config');
const userRepository = require('../../repositories/userRepository.js');
const { keysToCamel, keysToSnake } = require('../../helpers/object.converting.helper.js');


const getUserById = async(id) => {
    const user = await userRepository.getUserById(id);
    return keysToCamel(user); // keys helper here
};

const createUser = async(userToCreate) => {
    const { type, email, phone, name, city } = userToCreate;

    const createdUser = await userRepository.createUser({
        type,
        email,
        phone,
        name,
        city,
        balance: 0
    });

    const accessToken = jwt.sign({ id: createdUser.id, type: createdUser.type }, jwt_secret);

    createdUser.createdAt = createdUser.created_at;
    delete createdUser.created_at;
    createdUser.updatedAt = createdUser.updated_at;
    delete createdUser.updated_at;

    return {...createdUser, accessToken };
};

const updateUser = async(id, user) => {
    const updatedUser = await userRepository.updateUser(id, keysToSnake(user)); // keys helper here
    return keysToCamel(updatedUser); // keys helper here
};

module.exports = { getUserById, createUser, updateUser };