const betRepository = require('../../repositories/betRepository.js');
const { keysToSnake, keysToCamel } = require('../../helpers/object.converting.helper');

const getBetsByEventIdAndWin = async(eventId, win) => {
    const bets = await betRepository.getBetsByEventIdAndWin(eventId, win);
    return bets.map(keysToCamel);
};

const createBet = async(betComing) => {
    const { eventId, userId, betAmount, prediction, multiplier, win } = betComing;

    const createdBet = await betRepository.createBet(
        keysToSnake({
            eventId,
            userId,
            betAmount,
            prediction,
            multiplier,
            win,
        })
    );

    return keysToCamel(createdBet);
};

const updateBetById = async(id, bet) => {
    const updatedBet = await betRepository.updateBetById(id, keysToSnake(bet));
    return keysToCamel(updatedBet);
};

module.exports = { createBet, getBetsByEventIdAndWin, updateBetById };