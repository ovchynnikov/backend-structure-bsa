const oddsRepositories = require('../../db/repositories/odds.repositories.js');
const { keysToSnake, keysToCamel } = require('../../helpers/object.helper');

const getOddsById = async(id) => {
    const odds = await oddsRepositories.getOddsById(id);
    return keysToCamel(odds);
};

const createOdds = async(oddsDto) => {
    const { homeWin, draw, awayWin } = oddsDto;

    const createdOdds = await oddsDao.createOdds(
        keysToSnake({ homeWin, draw, awayWin })
    );

    return keysToCamel(createdOdds);
};

module.exports = { getOddsById, createOdds };