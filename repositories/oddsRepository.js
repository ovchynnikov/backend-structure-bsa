const db = require('../db/connect.js');

const table = 'odds';

const getOddsById = async(id) => {
    const odds = await db(table).where('id', id).returning('*');
    return odds.length ? odds[0] : null;
};

const createOdds = async(odds) => {
    const createdOdds = await db(table).insert(odds).returning('*');
    return createdOdds[0];
};

module.exports = { getOddsById, createOdds };