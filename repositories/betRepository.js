const db = require('../db/connect.js');

const table = 'bet';

const getWinBetsByEventId = async(eventId, win) => {
    return await db(table).where('event_id', eventId).andWhere('win', win);
};

const createBet = async(bet) => {
    const createdBet = await db(table).insert(bet).returning('*');
    return createdBet[0];
};

const updateBetById = async(id, bet) => {
    const updatedBet = await db(table).where('id', id).update(bet);
    return updatedBet.length > 0 ? updatedBet[0] : null;
};

module.exports = { createBet, getWinBetsByEventId, updateBetById };