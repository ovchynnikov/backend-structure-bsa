const { db } = require('../db/connect.js');

const table = 'user';

const createUser = async(user) => {
    const createdUser = await db(table).insert(user).returning('*');
    return createdUser[0];
};

const getUserById = async(id) => {
    const user = await db(table).where('id', id).returning('*');
    return user.length > 0 ? user[0] : null;
};

const updateUser = async(id, user) => {
    const updatedUser = await db(table).where('id', id).update(user).returning('*');
    return updatedUser.length > 0 ? updatedUser[0] : null;
};

module.exports = { getUserById, createUser, updateUser };