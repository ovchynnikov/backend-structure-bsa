const dbConfig = require("../knexfile");
const knex = require("knex");

exports.db = knex(dbConfig.development);