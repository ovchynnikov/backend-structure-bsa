const express = require("express");
const routes = require('./api/routes/index.js');
const { PORT } = require("./config/port.config.js");
const { db } = require("./db/connect.js");
const { EventEmitter } = require('events');
const stats = require("./stats/stats.js");

const app = express();

const statEmitter = new EventEmitter();

app.set('statEmitter', statEmitter);

app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
    db.raw('select 1+1 as result').then(function() {
        neededNext();
    }).catch(() => {
        throw new Error('No db connection');
    });
});

routes(app);
app.listen(PORT, () => {
    stats.listen(app)
    console.log(`App listening at http://localhost:${PORT}`);
});

// Do not change this line
module.exports = { app };