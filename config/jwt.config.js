const dotenv = require('dotenv');

dotenv.config();

const jwt_secret = process.env.JWT_SECRET;

module.exports = { jwt_secret };