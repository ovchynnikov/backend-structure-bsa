const snakeCaseToCamelCase = (string) => {
    return string.split('_').map((part, index) => (index ? part[0].toUpperCase() + part.slice(1) : part)).join('');
};

const camelCaseToSnakeCase = (string) => {
    return string.replace(/[A-Z]/g, (letter) => '_' + letter.toLowerCase());
};

const keysToCamel = (body) => {
    if (!body) {
        return null;
    }

    return Object.fromEntries(Object.entries(body).map(([key, value]) => [snakeCaseToCamelCase(key), value]));
};

const keysToSnake = (body) => {
    if (!body) {
        return null;
    }
    return Object.fromEntries(Object.entries(body).map(([key, value]) => [camelCaseToSnakeCase(key), value]));
};

module.exports = { keysToCamel, keysToSnake };