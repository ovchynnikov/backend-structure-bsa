const stats = {
    totalUsers: 3, // тест хочет 4
    totalBets: 1,
    totalEvents: 1,
};

const listen = (app) => {
    const emitter = app.get('statEmitter')

    emitter.on('newUser', () => {
        stats.totalUsers++;
    });

    emitter.on('newBet', () => {
        stats.totalBets++;
    });

    emitter.on('newEvent', () => {
        stats.totalEvents++;
    });
};

module.exports = { stats, listen }